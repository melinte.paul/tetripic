build:tema2

tema2:main.c
	gcc main.c -g -Wall -o tema2

run_task1:tema2
	./tema2 1

run_task2:tema2
	./tema2 2

run_task3:tema2
	./tema2 3

run_task4:tema2
	./tema2 4

run_bonus:tema2
	./tema2 5

clean:
	rm tema2 piesa_*