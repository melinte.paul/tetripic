#include <stdio.h>
#include "structs.h"

int main()
{
	piece pieces[28];

	//Piesa O
	//Rotati1 0
	pieces[0].size_x = 2; 
	pieces[0].size_y = 2;
	pieces[0].color.b = 0; 
	pieces[0].color.g = 255;
	pieces[0].color.r = 255;
	pieces[0].name = 'O';
	pieces[0].rot = 0;
	pieces[0].parts[0][0] = 0; pieces[0].parts[0][1] = 0; 
	pieces[0].parts[1][0] = 0; pieces[0].parts[1][1] = 1;
	pieces[0].parts[2][0] = 1; pieces[0].parts[2][1] = 0;
	pieces[0].parts[3][0] = 1; pieces[0].parts[3][1] = 1;

	//Rotati1 90
	pieces[1].size_x = 2; 
	pieces[1].size_y = 2;
	pieces[1].color.b = 0; 
	pieces[1].color.g = 255;
	pieces[1].color.r = 255;
	pieces[1].name = 'O';
	pieces[1].rot = 90;
	pieces[1].parts[0][0] = 0; pieces[1].parts[0][1] = 0; 
	pieces[1].parts[1][0] = 0; pieces[1].parts[1][1] = 1;
	pieces[1].parts[2][0] = 1; pieces[1].parts[2][1] = 0;
	pieces[1].parts[3][0] = 1; pieces[1].parts[3][1] = 1;

	//Rotati1 180
	pieces[2].size_x = 2; 
	pieces[2].size_y = 2;
	pieces[2].color.b = 0; 
	pieces[2].color.g = 255;
	pieces[2].color.r = 255;
	pieces[2].name = 'O';
	pieces[2].rot = 180;
	pieces[2].parts[0][0] = 0; pieces[2].parts[0][1] = 0; 
	pieces[2].parts[1][0] = 0; pieces[2].parts[1][1] = 1;
	pieces[2].parts[2][0] = 1; pieces[2].parts[2][1] = 0;
	pieces[2].parts[3][0] = 1; pieces[2].parts[3][1] = 1;

	//Rotati1 270
	pieces[3].size_x = 2; 
	pieces[3].size_y = 2;
	pieces[3].color.b = 0; 
	pieces[3].color.g = 255;
	pieces[3].color.r = 255;
	pieces[3].name = 'O';
	pieces[3].rot = 270;
	pieces[3].parts[0][0] = 0; pieces[3].parts[0][1] = 0; 
	pieces[3].parts[1][0] = 0; pieces[3].parts[1][1] = 1;
	pieces[3].parts[2][0] = 1; pieces[3].parts[2][1] = 0;
	pieces[3].parts[3][0] = 1; pieces[3].parts[3][1] = 1;

	//Piesa I
	//Rotati1 0
	pieces[4].size_x = 4; 
	pieces[4].size_y = 1;
	pieces[4].color.b = 255; 
	pieces[4].color.g = 0;
	pieces[4].color.r = 0;
	pieces[4].name = 'I';
	pieces[4].rot = 0;
	pieces[4].parts[0][0] = 0; pieces[4].parts[0][1] = 0; 
	pieces[4].parts[1][0] = 1; pieces[4].parts[1][1] = 0;
	pieces[4].parts[2][0] = 2; pieces[4].parts[2][1] = 0;
	pieces[4].parts[3][0] = 3; pieces[4].parts[3][1] = 0;

	//Rotati1 90
	pieces[5].size_x = 1; 
	pieces[5].size_y = 4;
	pieces[5].color.b = 255; 
	pieces[5].color.g = 0;
	pieces[5].color.r = 0;
	pieces[5].name = 'I';
	pieces[5].rot = 90;
	pieces[5].parts[0][0] = 0; pieces[5].parts[0][1] = 0; 
	pieces[5].parts[1][0] = 0; pieces[5].parts[1][1] = 1;
	pieces[5].parts[2][0] = 0; pieces[5].parts[2][1] = 2;
	pieces[5].parts[3][0] = 0; pieces[5].parts[3][1] = 3;

	//Rotati1 180
	pieces[6].size_x = 4; 
	pieces[6].size_y = 1;
	pieces[6].color.b = 255; 
	pieces[6].color.g = 0;
	pieces[6].color.r = 0;
	pieces[6].name = 'I';
	pieces[6].rot = 180;
	pieces[6].parts[0][0] = 0; pieces[6].parts[0][1] = 0; 
	pieces[6].parts[1][0] = 1; pieces[6].parts[1][1] = 0;
	pieces[6].parts[2][0] = 2; pieces[6].parts[2][1] = 0;
	pieces[6].parts[3][0] = 3; pieces[6].parts[3][1] = 0;

	//Rotati1 270
	pieces[7].size_x = 1; 
	pieces[7].size_y = 4;
	pieces[7].color.b = 255; 
	pieces[7].color.g = 0;
	pieces[7].color.r = 0;
	pieces[7].name = 'I';
	pieces[7].rot = 270;
	pieces[7].parts[0][0] = 0; pieces[7].parts[0][1] = 0; 
	pieces[7].parts[1][0] = 0; pieces[7].parts[1][1] = 1;
	pieces[7].parts[2][0] = 0; pieces[7].parts[2][1] = 2;
	pieces[7].parts[3][0] = 0; pieces[7].parts[3][1] = 3;

	//Piesa S
	//Rotati1 0
	pieces[8].size_x = 2; 
	pieces[8].size_y = 3;
	pieces[8].color.b = 0; 
	pieces[8].color.g = 0;
	pieces[8].color.r = 255;
	pieces[8].name = 'S';
	pieces[8].rot = 0;
	pieces[8].parts[0][0] = 0; pieces[8].parts[0][1] = 1; 
	pieces[8].parts[1][0] = 0; pieces[8].parts[1][1] = 2;
	pieces[8].parts[2][0] = 1; pieces[8].parts[2][1] = 0;
	pieces[8].parts[3][0] = 1; pieces[8].parts[3][1] = 1;

	//Rotati1 90
	pieces[9].size_x = 3; 
	pieces[9].size_y = 2;
	pieces[9].color.b = 0; 
	pieces[9].color.g = 0;
	pieces[9].color.r = 255;
	pieces[9].name = 'S';
	pieces[9].rot = 90;
	pieces[9].parts[0][0] = 0; pieces[9].parts[0][1] = 0; 
	pieces[9].parts[1][0] = 1; pieces[9].parts[1][1] = 0;
	pieces[9].parts[2][0] = 1; pieces[9].parts[2][1] = 1;
	pieces[9].parts[3][0] = 2; pieces[9].parts[3][1] = 1;

	//Rotati1 180
	pieces[10].size_x = 2; 
	pieces[10].size_y = 3;
	pieces[10].color.b = 0; 
	pieces[10].color.g = 0;
	pieces[10].color.r = 255;
	pieces[10].name = 'S';
	pieces[10].rot = 180;
	pieces[10].parts[0][0] = 0; pieces[10].parts[0][1] = 1; 
	pieces[10].parts[1][0] = 0; pieces[10].parts[1][1] = 2;
	pieces[10].parts[2][0] = 1; pieces[10].parts[2][1] = 0;
	pieces[10].parts[3][0] = 1; pieces[10].parts[3][1] = 1;

	//Rotati1 270
	pieces[11].size_x = 3; 
	pieces[11].size_y = 2;
	pieces[11].color.b = 0; 
	pieces[11].color.g = 0;
	pieces[11].color.r = 255;
	pieces[11].name = 'S';
	pieces[11].rot = 270;
	pieces[11].parts[0][0] = 0; pieces[11].parts[0][1] = 0; 
	pieces[11].parts[1][0] = 1; pieces[11].parts[1][1] = 0;
	pieces[11].parts[2][0] = 1; pieces[11].parts[2][1] = 1;
	pieces[11].parts[3][0] = 2; pieces[11].parts[3][1] = 1;

	//Piesa Z
	//Rotati1 0
	pieces[12].size_x = 2; 
	pieces[12].size_y = 3;
	pieces[12].color.b = 0; 
	pieces[12].color.g = 255;
	pieces[12].color.r = 0;
	pieces[12].name = 'Z';
	pieces[12].rot = 0;
	pieces[12].parts[0][0] = 0; pieces[12].parts[0][1] = 0; 
	pieces[12].parts[1][0] = 0; pieces[12].parts[1][1] = 1;
	pieces[12].parts[2][0] = 1; pieces[12].parts[2][1] = 1;
	pieces[12].parts[3][0] = 1; pieces[12].parts[3][1] = 2;

	//Rotati1 90
	pieces[13].size_x = 3; 
	pieces[13].size_y = 2;
	pieces[13].color.b = 0; 
	pieces[13].color.g = 255;
	pieces[13].color.r = 0;
	pieces[13].name = 'Z';
	pieces[13].rot = 90;
	pieces[13].parts[0][0] = 0; pieces[13].parts[0][1] = 1; 
	pieces[13].parts[1][0] = 1; pieces[13].parts[1][1] = 0;
	pieces[13].parts[2][0] = 1; pieces[13].parts[2][1] = 1;
	pieces[13].parts[3][0] = 2; pieces[13].parts[3][1] = 0;

	//Rotati1 180
	pieces[14].size_x = 2; 
	pieces[14].size_y = 3;
	pieces[14].color.b = 0; 
	pieces[14].color.g = 255;
	pieces[14].color.r = 0;
	pieces[14].name = 'Z';
	pieces[14].rot = 180;
	pieces[14].parts[0][0] = 0; pieces[14].parts[0][1] = 0; 
	pieces[14].parts[1][0] = 0; pieces[14].parts[1][1] = 1;
	pieces[14].parts[2][0] = 1; pieces[14].parts[2][1] = 1;
	pieces[14].parts[3][0] = 1; pieces[14].parts[3][1] = 2;

	//Rotati1 270
	pieces[15].size_x = 3; 
	pieces[15].size_y = 2;
	pieces[15].color.b = 0; 
	pieces[15].color.g = 255;
	pieces[15].color.r = 0;
	pieces[15].name = 'Z';
	pieces[15].rot = 270;
	pieces[15].parts[0][0] = 0; pieces[15].parts[0][1] = 1; 
	pieces[15].parts[1][0] = 1; pieces[15].parts[1][1] = 0;
	pieces[15].parts[2][0] = 1; pieces[15].parts[2][1] = 1;
	pieces[15].parts[3][0] = 2; pieces[15].parts[3][1] = 0;

	//Piesa L
	//Rotati1 0
	pieces[16].size_x = 3; 
	pieces[16].size_y = 2;
	pieces[16].color.b = 0; 
	pieces[16].color.g = 140;
	pieces[16].color.r = 255;
	pieces[16].name = 'L';
	pieces[16].rot = 0;
	pieces[16].parts[0][0] = 0; pieces[16].parts[0][1] = 0; 
	pieces[16].parts[1][0] = 1; pieces[16].parts[1][1] = 0;
	pieces[16].parts[2][0] = 2; pieces[16].parts[2][1] = 0;
	pieces[16].parts[3][0] = 2; pieces[16].parts[3][1] = 1;

	//Rotati1 90
	pieces[17].size_x = 2; 
	pieces[17].size_y = 3;
	pieces[17].color.b = 0; 
	pieces[17].color.g = 140;
	pieces[17].color.r = 255;
	pieces[17].name = 'L';
	pieces[17].rot = 90;
	pieces[17].parts[0][0] = 0; pieces[17].parts[0][1] = 0; 
	pieces[17].parts[1][0] = 0; pieces[17].parts[1][1] = 1;
	pieces[17].parts[2][0] = 0; pieces[17].parts[2][1] = 2;
	pieces[17].parts[3][0] = 1; pieces[17].parts[3][1] = 0;

	//Rotati1 180
	pieces[18].size_x = 3; 
	pieces[18].size_y = 2;
	pieces[18].color.b = 0; 
	pieces[18].color.g = 140;
	pieces[18].color.r = 255;
	pieces[18].name = 'L';
	pieces[18].rot = 180;
	pieces[18].parts[0][0] = 0; pieces[18].parts[0][1] = 0; 
	pieces[18].parts[1][0] = 0; pieces[18].parts[1][1] = 1;
	pieces[18].parts[2][0] = 1; pieces[18].parts[2][1] = 1;
	pieces[18].parts[3][0] = 2; pieces[18].parts[3][1] = 1;

	//Rotati1 270
	pieces[19].size_x = 2; 
	pieces[19].size_y = 3;
	pieces[19].color.b = 0; 
	pieces[19].color.g = 140;
	pieces[19].color.r = 255;
	pieces[19].name = 'L';
	pieces[19].rot = 270;
	pieces[19].parts[0][0] = 0; pieces[19].parts[0][1] = 2; 
	pieces[19].parts[1][0] = 1; pieces[19].parts[1][1] = 0;
	pieces[19].parts[2][0] = 1; pieces[19].parts[2][1] = 1;
	pieces[19].parts[3][0] = 1; pieces[19].parts[3][1] = 2;

	//Piesa J
	//Rotati1 0
	pieces[20].size_x = 3; 
	pieces[20].size_y = 2;
	pieces[20].color.b = 255; 
	pieces[20].color.g = 0;
	pieces[20].color.r = 255;
	pieces[20].name = 'J';
	pieces[20].rot = 0;
	pieces[20].parts[0][0] = 0; pieces[20].parts[0][1] = 1; 
	pieces[20].parts[1][0] = 1; pieces[20].parts[1][1] = 1;
	pieces[20].parts[2][0] = 2; pieces[20].parts[2][1] = 0;
	pieces[20].parts[3][0] = 2; pieces[20].parts[3][1] = 1;

	//Rotati1 90
	pieces[21].size_x = 2; 
	pieces[21].size_y = 3;
	pieces[21].color.b = 255; 
	pieces[21].color.g = 0;
	pieces[21].color.r = 255;
	pieces[21].name = 'J';
	pieces[21].rot = 90;
	pieces[21].parts[0][0] = 0; pieces[21].parts[0][1] = 0; 
	pieces[21].parts[1][0] = 1; pieces[21].parts[1][1] = 0;
	pieces[21].parts[2][0] = 1; pieces[21].parts[2][1] = 1;
	pieces[21].parts[3][0] = 1; pieces[21].parts[3][1] = 2;

	//Rotati1 180
	pieces[22].size_x = 3; 
	pieces[22].size_y = 2;
	pieces[22].color.b = 255; 
	pieces[22].color.g = 0;
	pieces[22].color.r = 255;
	pieces[22].name = 'J';
	pieces[22].rot = 180;
	pieces[22].parts[0][0] = 0; pieces[22].parts[0][1] = 0; 
	pieces[22].parts[1][0] = 0; pieces[22].parts[1][1] = 1;
	pieces[22].parts[2][0] = 1; pieces[22].parts[2][1] = 0;
	pieces[22].parts[3][0] = 2; pieces[22].parts[3][1] = 0;

	//Rotati1 270
	pieces[23].size_x = 2; 
	pieces[23].size_y = 3;
	pieces[23].color.b = 255; 
	pieces[23].color.g = 0;
	pieces[23].color.r = 255;
	pieces[23].name = 'J';
	pieces[23].rot = 270;
	pieces[23].parts[0][0] = 0; pieces[23].parts[0][1] = 0; 
	pieces[23].parts[1][0] = 0; pieces[23].parts[1][1] = 1;
	pieces[23].parts[2][0] = 0; pieces[23].parts[2][1] = 2;
	pieces[23].parts[3][0] = 1; pieces[23].parts[3][1] = 2;

	//Piesa T
	//Rotati1 0
	pieces[24].size_x = 2; 
	pieces[24].size_y = 3;
	pieces[24].color.b = 255; 
	pieces[24].color.g = 0;
	pieces[24].color.r = 130;
	pieces[24].name = 'T';
	pieces[24].rot = 0;
	pieces[24].parts[0][0] = 0; pieces[24].parts[0][1] = 0; 
	pieces[24].parts[1][0] = 0; pieces[24].parts[1][1] = 1;
	pieces[24].parts[2][0] = 0; pieces[24].parts[2][1] = 2;
	pieces[24].parts[3][0] = 1; pieces[24].parts[3][1] = 1;

	//Rotati1 90
	pieces[25].size_x = 3; 
	pieces[25].size_y = 2;
	pieces[25].color.b = 255; 
	pieces[25].color.g = 0;
	pieces[25].color.r = 130;
	pieces[25].name = 'T';
	pieces[25].rot = 90;
	pieces[25].parts[0][0] = 0; pieces[25].parts[0][1] = 1; 
	pieces[25].parts[1][0] = 1; pieces[25].parts[1][1] = 0;
	pieces[25].parts[2][0] = 1; pieces[25].parts[2][1] = 1;
	pieces[25].parts[3][0] = 2; pieces[25].parts[3][1] = 1;

	//Rotati1 180
	pieces[26].size_x = 2; 
	pieces[26].size_y = 3;
	pieces[26].color.b = 255; 
	pieces[26].color.g = 0;
	pieces[26].color.r = 130;
	pieces[26].name = 'T';
	pieces[26].rot = 180;
	pieces[26].parts[0][0] = 0; pieces[26].parts[0][1] = 1; 
	pieces[26].parts[1][0] = 1; pieces[26].parts[1][1] = 0;
	pieces[26].parts[2][0] = 1; pieces[26].parts[2][1] = 1;
	pieces[26].parts[3][0] = 1; pieces[26].parts[3][1] = 2;

	//Rotati1 270
	pieces[27].size_x = 3; 
	pieces[27].size_y = 2;
	pieces[27].color.b = 255; 
	pieces[27].color.g = 0;
	pieces[27].color.r = 130;
	pieces[27].name = 'T';
	pieces[27].rot = 270;
	pieces[27].parts[0][0] = 0; pieces[27].parts[0][1] = 0; 
	pieces[27].parts[1][0] = 1; pieces[27].parts[1][1] = 0;
	pieces[27].parts[2][0] = 1; pieces[27].parts[2][1] = 1;
	pieces[27].parts[3][0] = 2; pieces[27].parts[3][1] = 0;

	FILE *bin_pieces=fopen("pieces.bin","wb");

	fwrite(&pieces,sizeof(piece),28,bin_pieces);

	return 0;
}