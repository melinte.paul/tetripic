#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "bmp_header.h"
#include "structs.h"

//TODO remove -g flag from make
//TODO fclose all files
//TODO scade liniile la 80
//TODO clean rule

pixel** bmpToMap(char *filename, int *height, int *width)
{
	FILE *in=fopen(filename, "rb");

	struct bmp_fileheader file_header;
	struct bmp_infoheader info_header;
	fread(&file_header,sizeof(struct bmp_fileheader),1,in);
	fread(&info_header,sizeof(struct bmp_infoheader),1,in);
	fseek(in,file_header.imageDataOffset,SEEK_SET);

	*height=info_header.height/10;
	*width=info_header.width/10;
	int i,j;

	pixel **pixel_map=malloc(*height*sizeof(pixel*));
	for(i=0;i<*height;i++)
	{
		pixel_map[i]=malloc(*width*sizeof(pixel));
	}

	for(i=*height-1;i>=0;i--)
	{
		for(j=0;j<*width;j++)
		{
			fread(&pixel_map[i][j],sizeof(pixel),1,in);
			fseek(in,sizeof(pixel)*9,SEEK_CUR);
		}
		fseek(in,((sizeof(pixel)*(*width)*10)%4)*10+(sizeof(pixel)*(*width)*10)*9,SEEK_CUR);
	}

	return pixel_map;
}

void mapToBmp(pixel **pixel_map,int height, int width, char *filename)
{
	int i,j,k,l;
	struct bmp_fileheader new_file_header={'B','M',0,0,0,54};
	struct bmp_infoheader new_info_header={40,width*10,height*10,1,24,0,0,0,0,0,0};

	new_info_header.biSizeImage=(sizeof(pixel)*width*10+(sizeof(pixel)*width*10)%4)*height*10;
	new_file_header.bfSize=new_info_header.biSizeImage+54;

	FILE *out = fopen(filename, "wb");

	fwrite(&new_file_header,sizeof(struct bmp_fileheader),1,out);
	fwrite(&new_info_header,sizeof(struct bmp_infoheader),1,out);

	for(i=height-1;i>=0;i--)
	{
		for(k=0;k<10;k++)
		{
			for(j=0;j<width;j++)
			{
				for(l=0;l<10;l++)
				{
					fwrite(&pixel_map[i][j],sizeof(pixel),1,out);
				}
			}
			if((sizeof(pixel)*width*10)%4!=0)
			{
				char aux='\0';
				fwrite(&aux,sizeof(char),1,out);
				fwrite(&aux,sizeof(char),1,out);
			}

		}
	}
}

void print_piece(piece p,char *name)
{
	pixel **pixel_map=malloc((p.height+2)*sizeof(pixel*));
	int i,j;

	for(i=0;i<p.height+2;i++)
	{
		pixel_map[i]=malloc((p.width+2)*sizeof(pixel));
	}

	for(i=0;i<p.height+2;i++)
		for(j=0;j<p.width+2;j++)
		{
			pixel_map[i][j].r=255;
			pixel_map[i][j].g=255;
			pixel_map[i][j].b=255;
		}

	for(i=0;i<4;i++)
	{
		pixel_map[p.parts[i][0]+1][p.parts[i][1]+1].r=p.color.r;
		pixel_map[p.parts[i][0]+1][p.parts[i][1]+1].g=p.color.g;
		pixel_map[p.parts[i][0]+1][p.parts[i][1]+1].b=p.color.b;
	}

	mapToBmp(pixel_map,p.height+2,p.width+2,name);

	//TODO dezalocare
}

void task1()
{
	FILE *bin_parts=fopen("pieces.bin","rb");
	piece pieces[28];
	fread(&pieces,sizeof(piece),28,bin_parts);

	int i;

	for(i=0;i<28;i+=4)
	{
		char name[20] = "piesa_";
		char piece_name[2];
		piece_name[0]=pieces[i].name;
		piece_name[1]='\0';
		strcat(name,piece_name);
		strcat(name,".bmp");

		print_piece(pieces[i],name);
	}

}

void task2()
{
	FILE *bin_parts=fopen("pieces.bin","rb");
	piece pieces[28];
	fread(&pieces,sizeof(piece),28,bin_parts);

	int i;

	for(i=0;i<28;i++)
	{
		if(i%4==0)
		{
			continue;
		}
		
		char name[20] = "piesa_";

		char piece_name[2];
		piece_name[0]=pieces[i].name;
		piece_name[1]='\0';
		strcat(name,piece_name);
		strcat(name,"_");

		int rot=pieces[i].rot;
		switch(rot)
		{
			case(0):
				strcat(name,"0");
				break;
			case(90):
				strcat(name,"90");
				break;
			case(180):
				strcat(name,"180");
				break;
			case(270):
				strcat(name,"270");
				break;
		}

		strcat(name,".bmp");

		print_piece(pieces[i],name);
	}

}

void make_white_row(pixel* row, int width)
{
	int i;

	for(i=0;i<width;i++)
	{
		row[i].r=255;
		row[i].g=255;
		row[i].b=255;
	}
}

int is_black(pixel p)
{
	return p.r==0 && p.g==0 && p.b==0;
}

int is_line_full(pixel* row, int width)
{
	int i;

	for(i=0;i<width;i++)
	{
		if(is_black(row[i]))
		{
			return 0;
		}
	}

	return 1;
}

void sim_tetris(pixel **pixel_map, int height, int width, FILE *in, int moves)
{
	int i,j,piece_rot,col,aux,row;
	char piece_type;
	piece *p;

	FILE *bin_parts=fopen("pieces.bin","rb");
	piece pieces[28];
	fread(&pieces,sizeof(piece),28,bin_parts);

	for(i=0;i<moves;i++)
	{
		fscanf(in," %c%d%d",&piece_type,&piece_rot,&col);
		switch(piece_type)
		{
			case('O'):
				aux=0;
				break;
			case('I'):
				aux=4;
				break;
			case('S'):
				aux=8;
				break;
			case('Z'):
				aux=12;
				break;
			case('L'):
				aux=16;
				break;
			case('J'):
				aux=20;
				break;
			case('T'):
				aux=24;
				break;
		}

		switch(piece_rot)
		{
			case(0):
				break;
			case(90):
				aux+=1;
				break;
			case(180):
				aux+=2;
				break;
			case(270):
				aux+=3;
				break;
		}

		p=&pieces[aux];

		aux=1;

		for(row=0;row<(height-p->height+1) && aux;row++)
		{
			for(j=0;j<4 && aux;j++)
			{
				if(row+p->parts[j][0]<4)
				{
					continue;
				}

				if(pixel_map[row+p->parts[j][0]][col+p->parts[j][1]].r!=0 || 
					pixel_map[row+p->parts[j][0]][col+p->parts[j][1]].g!=0 || 
					pixel_map[row+p->parts[j][0]][col+p->parts[j][1]].b!=0)
				{
					row--;
					aux=0;
				}
			}	
			
		}

		row--;
		
		aux=1;

		for(j=0;j<4;j++)
		{
			pixel_map[row+p->parts[j][0]][col+p->parts[j][1]].r=p->color.r;
			pixel_map[row+p->parts[j][0]][col+p->parts[j][1]].g=p->color.g;
			pixel_map[row+p->parts[j][0]][col+p->parts[j][1]].b=p->color.b;

			if(row+p->parts[j][0] < 4)
			{
				aux=0;
			}
		}

		if(aux==0)
		{
			break;
		}

		for(j=4;j<height;j++)
		{
			if(is_line_full(pixel_map[j],width))
			{
				printf("%d\n", j);
				free(pixel_map[j]);
				for(aux=j;aux>0;aux--)
				{
					pixel_map[aux]=pixel_map[aux-1];
				}
				pixel_map[0]=malloc(width*sizeof(pixel));
				
				make_white_row(pixel_map[0],width);
				for(aux=0;aux<width;aux++)
				{
					pixel_map[4][aux].r=0;
					pixel_map[4][aux].g=0;
					pixel_map[4][aux].b=0;
				}
			}
		}

	}
}

void task3()
{
	FILE *in=fopen("cerinta3.in","r");

	int moves,height,width,i;
	fscanf(in,"%d%d%d",&moves,&height,&width);
	height+=4;

	pixel **pixel_map=malloc(height*sizeof(pixel*));
	for(i=0;i<height;i++)
	{
		pixel_map[i]=malloc(width*sizeof(pixel));
	}
	for(i=0;i<4;i++)
	{
		make_white_row(pixel_map[i],width);
	}

	sim_tetris(pixel_map,height,width,in,moves);

	mapToBmp(pixel_map,height,width,"task3.bmp");

	//TODO dezalocare
}

void task4()
{
	int height,width;

	pixel **pixel_map=bmpToMap("cerinta4.bmp",&height,&width);

	FILE *in=fopen("cerinta4.in","r");

	int moves;

	fscanf(in,"%d",&moves);

	sim_tetris(pixel_map,height,width,in,moves);

	mapToBmp(pixel_map,height,width,"task4.bmp");
	//TODO dezalocare
}

void task5()
{
	FILE *bin_parts=fopen("pieces.bin","rb");
	piece pieces[28];
	fread(&pieces,sizeof(piece),28,bin_parts);

	int height,width;
	pixel **pixel_map=bmpToMap("bonus.bmp",&height,&width);

	int count=0,i,j,k,l,aux;
	char pieces_name[100];
	int pieces_rot[100],pieces_col[100];

	for(i=4;i<height;i++)
		for(j=0;j<width;j++)
		{
			for(k=0;k<28;k++)
			{
				aux=1;
				if(i+pieces[k].height > height)
				{
					continue;
				}
				if(j+pieces[k].width > width)
				{
					continue;
				}

				for(l=0;l<4;l++)
				{
					if(pixel_map[i+pieces[k].parts[l][0]][j+pieces[k].parts[l][1]].r!=pieces[k].color.r ||
						pixel_map[i+pieces[k].parts[l][0]][j+pieces[k].parts[l][1]].g!=pieces[k].color.g ||
						pixel_map[i+pieces[k].parts[l][0]][j+pieces[k].parts[l][1]].b!=pieces[k].color.b)
					{
						aux=0;
					}
				}

				if(aux==1)
				{
					pieces_name[count]=pieces[k].name;
					pieces_rot[count]=pieces[k].rot;
					pieces_col[count]=j;
					count++;
					for(l=0;l<4;l++)
					{
					pixel_map[i+pieces[k].parts[l][0]][j+pieces[k].parts[l][1]].r=0;
					pixel_map[i+pieces[k].parts[l][0]][j+pieces[k].parts[l][1]].g=0;
					pixel_map[i+pieces[k].parts[l][0]][j+pieces[k].parts[l][1]].b=0;
					}
				}
			}
			
		}

	FILE *out=fopen("bonus.out","w");

	fprintf(out, "%d\n", count);

	for(i=0;i<count;i++)
	{
		fprintf(out, "%c %d %d\n", pieces_name[i],pieces_rot[i],pieces_col[i]);
	}

	//TODO dezalocare
}

int main(int argc, char **argv)
{
	if(argc == 1)
	{
		printf("Please select task to complete\n");
		return 1;
	}

	switch(argv[1][0])
	{
		case '1':
			task1();
			break;
		case '2':
			task2();
			break;
		case '3':
			task3();
			break;
		case '4':
			task4();
			break;
		case '5':
			task5();
			break;
		default:
			printf("Please select a valid task\n");
	}
	return 0;
}