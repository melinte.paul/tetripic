#ifndef __STRUCTS__
#define __STRUCTS__

#pragma pack(1)

typedef struct
{
	unsigned char b,g,r;
}pixel;

typedef struct 
{
	char height,width;
	pixel color;
	char name;
	int rot;
	signed char parts[4][2];
}piece;

#pragma pack()

#endif